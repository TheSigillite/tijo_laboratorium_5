package pl.edu.pwsztar.domain.mapper;

@FunctionalInterface
public interface MapperFunctionInterface<T, F> {
    T convert(F from);
}
